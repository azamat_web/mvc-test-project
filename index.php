<?php
require 'application/bootstrap.php';
require __DIR__ . '/vendor/autoload.php';
use application\core\Router;
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$route = new Router;
$route->run();
