<?php

namespace application\models;

use application\core\Model;

class Task extends Model {

  function getTasks() {
    return $this->db->findAllTasks('tasks');
  }

  function addTask($data) {
    if ($this->db->insertTask('tasks', $data)) {

    }
  }

  function getTaskByid($id) {
    return $this->db->findOneTask('tasks', $id);
  }

  function updateTask($id, $description) {
      return $this->db->updateTask('tasks', $id, $description);
  }
}
