<?php

namespace application\lib;

use PDO;

class Db {

  protected static $db;

  function __construct() {
    
    if (self::$db == null) {
     self::$db = new PDO("mysql:host=" . $_ENV['HOST'] . ";dbname=" . $_ENV['DATABASE'] . ";charset=utf8", $_ENV['USER'], $_ENV['PASSWORD']);
      self::$db->exec('SET NAMES utf8');
		}
  }

  public function findAllTasks($table) {

    $sql = "SELECT * FROM " . $table;

    if(!empty($_SESSION['sort'])) {
        $sql = "SELECT * FROM " . $table . " ORDER BY " . $_SESSION['sort'];
    } else {
      $sql = "SELECT * FROM " . $table . " ORDER BY name";

    }
    $query = self::$db->prepare($sql);
    $query->execute();
    return $query->fetchAll();
  }

  public function insertTask($table, $data) {

    $sql = "INSERT INTO $table (name, email, description, status) VALUES (:name, :email, :description, :status)";
    $query = self::$db->prepare($sql);
    $query->execute($data);
  }

  public function updateTask($table, $id, $description) {

    $sql = "UPDATE $table SET description = '".$description."', status = '2' WHERE id = ".$id."";
    $query = self::$db->prepare($sql);
    $query->execute();
  }

  public function findUserbyLoginAndPass($table, $login, $password) {

    $sql = "SELECT * FROM $table WHERE login = '". $login ."' AND password = '". $password ."'";
    $query = self::$db->prepare($sql);
    $query->execute();
    return $query->fetchAll();
  }

  public function findOneTask($table, $id) {

    $sql = "SELECT * FROM $table WHERE id = ".$id;
    $query = self::$db->prepare($sql);
    $query->execute();
    return $query->fetchAll();
  }
}
