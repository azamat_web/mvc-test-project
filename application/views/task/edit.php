<?php
if ($task[0]['status'] == 1) {
  $checked = 'checked';
} else $checked = '';
 ?>
<div class="container">
  <form action="/task/update" id="taskForm" method="post">
    <input type="hidden" name="id" value="<?=$task[0]['id']?>">
   <div class="form-group">
     <label for="text" class="col-form-label">Текст:</label>
     <textarea name="description" required class="form-control" rows="9" id="text"><?=$task[0]['description']?></textarea>
   </div>
     <input type="submit" class="btn btn-primary" value="Save">
  </form>
</div>
