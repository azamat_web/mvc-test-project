<?php
  if(isset($_SESSION['status']) == 1):
?>
  <div class="alert alert-success" id="alert-btn" role="alert">
    Успешно добавлена!
  </div>
<?php
  unset($_SESSION['status']);
 endif;
?>
<div class="container">
  <table class="table table-sm">
    <thead>
      <div class="col-md-2" style="float:right;">
         <select id="sort" class="form-control">
          <option <?= $_SESSION['sort'] == 'id' ? 'selected' : '' ?> value="id">id</option>
          <option <?= $_SESSION['sort'] == 'name' ? 'selected' : '' ?> value="name">name</option>
          <option <?= $_SESSION['sort'] == 'email' ? 'selected' : '' ?> value="email">email</option>
        </select>
      </div>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Название</th>
        <th scope="col">Почта</th>
        <th scope="col">Текст</th>
        <th scope="col">Статус</th>
        <th scope="col"></th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($tasks as $key => $value): ?>
      <tr>
        <th scope="row"><?=$value['id']?></th>
        <td><?=$value['name']?></td>
        <td><?=$value['email']?></td>
        <td><?=$value['description']?></td>
        <?php if($value['status'] == 1): ?>
          <td>
            <p>выполнено</p>
          </td>
        <?php elseif($value['status'] == 2): ?>
          <td>
            <p>выполнено</p>
            <p style="color:#dc1212">отредактировано администратором</p>
          </td>
        <?php endif; ?>
        <?php if (!empty($_SESSION['login'])): ?>
        <td>
          <a type="button" href="task/edit/<?=$value['id']?>" class="btn btn-primary">edit</a>
        </td>
        <?php endif; ?>
      </tr>
      <?php endforeach;?>
    </tbody>
  </table>
  <a href="task/create" class="btn btn-primary">Создать задачу</a>
</div>

<script type="text/javascript">
  if( $('#alert-btn').is(':visible') ) {
    window.setTimeout(elementHide, 2000);
  }

  function elementHide() {
    document.getElementById('alert-btn').style.display = 'none';
  }

  $( "#sort" ).change(function() {
    var val = $( "#sort" ).val();
    $.post( "/task/sort", { value: val})
    .done(function( data ) {
      window.location.replace("/");
    });
});

</script>
