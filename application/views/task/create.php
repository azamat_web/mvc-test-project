<div class="container">
  <form action="/task/create" id="taskForm" method="post">
   <div class="form-group">
     <label for="name" class="col-form-label">Название:</label>
     <input type="text" name="name" required class="form-control" id="name">
   </div>
   <div class="form-group">
     <label for="email" class="col-form-label">Email:</label>
     <input type="email" name="email" required class="form-control" id="email">
   </div>
   <div class="form-group">
     <label for="text" class="col-form-label">Текст:</label>
     <textarea name="description" required class="form-control" rows="9" id="text"></textarea>
   </div>
     <input type="submit" class="btn btn-primary" value="Save">
  </form>
</div>
