<div class="container">
  <form action="login" id="loginForm" method="post">
    <div class="form-group">
      <label for="login">Login</label>
      <input type="text" name="login" required class="form-control" id="login" placeholder="Enter login">
      <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
    </div>
    <div class="form-group">
      <label for="password">Password</label>
      <input type="password" name="password" required class="form-control" id="password" placeholder="Password">
    </div>
    <?php if(!empty($_SESSION['error'])): ?>
      <div class="alert alert-danger" role="alert">
        error user or password
      </div>
    <?php endif; ?>
    <button type="submit" class="btn btn-primary">Войти</button>
  </form>
</div>
