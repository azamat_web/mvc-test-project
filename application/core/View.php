<?php

namespace application\core;

class View {

public $path;
public $route;
public $layout = 'default';

  public function __construct($route) {

    $this->route = $route;
    $this->path = $route['controller'] .'/'. $route['action'];
  }

  public function render($title, $vars = []) {

      extract($vars);
      $file = 'application/views/' . $this->path . '.php';
      if (file_exists($file)) {
        ob_start();
        require $file;
        $content = ob_get_contents();
        ob_end_clean();
      };
    require 'application/views/layouts/' . $this->layout . '.php';
  }

  public static function errorCode($code) {

    http_response_code($code);
    $file = 'application/views/errors/' . $code . '.php';
    if (file_exists($file)) {
        ob_start();
      require $file;
      $content = ob_get_contents();
      ob_end_clean();
      require 'application/views/layouts/default.php';
    }
    exit;
  }

  public function redirect($url) {

    header('location: ' . $url);
    exit;
  }
}
