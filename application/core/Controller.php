<?php

namespace application\core;

use application\core\View;

abstract class Controller {

  public $route;
  public $view;

  function __construct($route) {
    
    session_start();
    $this->route = $route;
    $this->view = new View($route);
  }
}
