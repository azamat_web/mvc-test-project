<?php

namespace application\core;

use application\core\View;

class Router {

  protected $routes = [];
  protected $params = [];

  public function __construct() {

    $this->load();
  }

  public function add($route, $params) {

    $route = '#^' . $route . '$#';
    $this->routes[$route] = $params;
  }

  public function match() {

    $url = trim($_SERVER['REQUEST_URI'], '/');
    foreach ($this->routes as $route => $params) {
      if (preg_match($route, $url, $matches)) {
        $this->params = $params;
        return true;
      } elseif (preg_match('#^(\w+)/(\w+)/(\d+)$#', $url, $matches)) {
        $this->params = ['controller' => $matches[1], 'action' => $matches[2]];
        return true;
      }
    }
    return false;
  }

  private function load() {

    $arr = require 'application/config/routes.php';
    foreach ($arr as $key => $value) {
      $this->add($key, $value);
    }

  }

  public function run() {
    
    if ($this->match()) {
      $path = 'application\controllers\\' . ucfirst($this->params['controller']) . 'Controller';
      if (class_exists($path)) {
        $action = $this->params['action'] . 'Action';

        if (method_exists($path, $action)) {
          $controller = new $path($this->params);
          $controller->$action();
        } else View::errorCode(404);
      } else View::errorCode(404);
    } else View::errorCode(404);
  }
}
