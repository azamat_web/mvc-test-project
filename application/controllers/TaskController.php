<?php

namespace application\controllers;

use application\core\Controller;
use application\models\Task;
use application\core\View;

class TaskController extends Controller {

  public function indexAction() {
    $model = new Task;
    $data = ['tasks' => $model->getTasks()];

    $this->view->render('Список задач', $data);
  }

  public function createAction() {

    if (!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['description'])) {

      $data = [
        'name' => htmlspecialchars($_POST['name']),
        'email' => htmlspecialchars($_POST['email']),
        'description' => htmlspecialchars($_POST['description']),
        'status' => 1,
      ];
      $model = new Task;
      $model->addTask($data);
      $_SESSION['status'] = 1;
      $this->view->redirect('/');
    }
    $this->view->render('Создать задачу');

  }

  public function editAction() {

    if(!empty($_SESSION['login'])) {
      $model = new Task;
      $url = explode("/", $_SERVER['REQUEST_URI']);
      $id = isset($url[3]) ? $url[3] : false;
      if ($id) {
        $data = ['task' => $model->getTaskByid($id)];
        $this->view->render('Изменить задачу', $data);
      }
    } else View::errorCode(401);
  }

  public function updateAction() {

      if (!empty($_POST['id']) && !empty($_POST['description'])) {
        $id = htmlspecialchars($_POST['id']);
        $description = htmlspecialchars($_POST['description']);
        $model = new Task;
        $model->updateTask($id, $description);
        $this->view->redirect('/');
      }
    View::errorCode(404);
  }

  public function sortAction() {
    if (!empty($_POST['value'])) {
      $_SESSION['sort'] = $_POST['value'];
    }
    echo json_encode($_POST);
  }
}
