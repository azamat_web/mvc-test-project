<?php

namespace application\controllers;

use application\core\Controller;
use application\models\Accaunt;

class AccauntController extends Controller {

  public function loginAction() {

    $model = new Accaunt;
    unset($_SESSION['error']);
    if (!empty($_SESSION['login'])) {
      $this->view->redirect('/');
    }
    if (!empty($_POST)) {
      $login = htmlspecialchars($_POST['login']);
      $password = htmlspecialchars($_POST['password']);
      if($model->checkUser($login, $password)) {
         $_SESSION['login'] = $login;
         unset($_SESSION['error']);
         return $this->view->redirect('/');
      } else {
        $_SESSION['error'] = 'error user or password';
      }
    }
    $this->view->render('Страница логина');
  }

  public function logoutAction() {

     if(session_destroy()) {
       $this->view->redirect('/');
      }
  }
}
