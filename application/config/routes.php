<?php

return  [
  '' => [
    'controller' => 'task',
    'action' => 'index',
  ],
  'task/create' => [
    'controller' => 'task',
    'action' => 'create',
  ],
  'task/edit' => [
    'controller' => 'task',
    'action' => 'edit',
  ],
  'task/update' => [
    'controller' => 'task',
    'action' => 'update',
  ],
  'task/sort' => [
    'controller' => 'task',
    'action' => 'sort',
  ],
  'login' => [
    'controller' => 'accaunt',
    'action' => 'login',
  ],
  'logout' => [
    'controller' => 'accaunt',
    'action' => 'logout',
  ],
  'register' => [
    'controller' => 'accaunt',
    'action' => 'register',
  ],
];
